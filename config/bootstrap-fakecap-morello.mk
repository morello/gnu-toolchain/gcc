# This option enables the Morello fake capability option for stage2 and stage3.
STAGE2_CFLAGS += -mfake-capability
STAGE3_CFLAGS += -mfake-capability
