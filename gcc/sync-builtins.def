/* This file contains the definitions and documentation for the
   synchronization builtins used in the GNU compiler.
   Copyright (C) 2005-2020 Free Software Foundation, Inc.

This file is part of GCC.

GCC is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation; either version 3, or (at your option) any later
version.

GCC is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with GCC; see the file COPYING3.  If not see
<http://www.gnu.org/licenses/>.  */

/* Before including this file, you should define a macro:

     DEF_SYNC_BUILTIN (ENUM, NAME, TYPE, ATTRS)

   See builtins.def for details.

   In the comments below, DSIZE is one of the following scalar sizes,
   where the numbers count bytes:

   - I1
   - I2
   - I4
   - I8
   - I16
   - ICAP (associated with intcap_t, so includes a tag bit)

   DOFF is the non-capability form of DSIZE, so that DOFF is ICAPOFF
   when DSIZE is ICAP.  */

/* Like DEF_SYNC_BUILTIN, but define capability and non-capability forms.  */
#define DEF_SYNC_BUILTIN_C(ENUM, NAME, TYPE, ATTRS) \
  DEF_SYNC_BUILTIN (ENUM, NAME, TYPE, ATTRS) \
  DEF_SYNC_BUILTIN (ENUM ## _C, NAME "_c", TYPE ## _C, ATTRS)

/* Generate DSIZE-specific functions for ENUM and NAME by invoking:

     DEF (ENUM', NAME', DSIZE, DOFF)

   for each available non-capability DSIZE.  The order of these definitions is
   important, since some code adds to or subtracts from the enum value.  */
#define FOR_NONCAP_SYNC_N(ENUM, NAME, DEF) \
  DEF (ENUM##_1, NAME "_1", I1, I1) \
  DEF (ENUM##_2, NAME "_2", I2, I2) \
  DEF (ENUM##_4, NAME "_4", I4, I4) \
  DEF (ENUM##_8, NAME "_8", I8, I8) \
  DEF (ENUM##_16, NAME "_16", I16, I16)

/* As above, but include ICAP.  */
#define FOR_ALL_SYNC_N(ENUM, NAME, DEF) \
  FOR_NONCAP_SYNC_N (ENUM, NAME, DEF) \
  DEF (ENUM##_CAPABILITY, NAME "_capability", ICAP, ICAPOFF)

/* Case statements for everything defined by FOR_NONCAP_SYNC_N.  */
#define CASE_SYNC_BUILTIN_NONCAP_N(ENUM) \
  case ENUM##_1: \
  case ENUM##_1_C: \
  case ENUM##_2: \
  case ENUM##_2_C: \
  case ENUM##_4: \
  case ENUM##_4_C: \
  case ENUM##_8: \
  case ENUM##_8_C: \
  case ENUM##_16: \
  case ENUM##_16_C

/* Case statements for everything defined by FOR_ALL_SYNC_N.  */
#define CASE_SYNC_BUILTIN_ALL_N(ENUM) \
  CASE_SYNC_BUILTIN_NONCAP_N (ENUM): \
  case ENUM##_CAPABILITY: \
  case ENUM##_CAPABILITY_C

/* The macros below operate in pairs.  The first macro defines functions
   for a particular DSIZE and the second macro defines the following:

   - an overloaded _N function that handles all relevant DSIZEs
   - a list of DSIZE-specific functions

   The second macro typically gives a sequence of enums like:

   - BUILT_IN_SYNC_FETCH_AND_ADD_N
   - BUILT_IN_SYNC_FETCH_AND_ADD_1
   - BUILT_IN_SYNC_FETCH_AND_ADD_1_C
   - BUILT_IN_SYNC_FETCH_AND_ADD_2
   - BUILT_IN_SYNC_FETCH_AND_ADD_2_C
   - BUILT_IN_SYNC_FETCH_AND_ADD_4
   - BUILT_IN_SYNC_FETCH_AND_ADD_4_C
   - BUILT_IN_SYNC_FETCH_AND_ADD_8
   - BUILT_IN_SYNC_FETCH_AND_ADD_8_C
   - BUILT_IN_SYNC_FETCH_AND_ADD_16
   - BUILT_IN_SYNC_FETCH_AND_ADD_16_C
   - BUILT_IN_SYNC_FETCH_AND_ADD_CAPABILITY
   - BUILT_IN_SYNC_FETCH_AND_ADD_CAPABILITY_C

   although not all functions have a CAPABILITY form.

   The _N function is only used as a function by the front ends, which resolve
   it to one of the DSIZE-specific functions.  However, the enum for the _N
   function is also a convenient way of referring to the group as a whole,
   and code outside the front ends uses it for that purpose.

   Various bits of code rely on the relationship between the enum values.
   For example, adding log2(nbytes) * 2 + 1 to an _N function gives
   (and must give) the associated DSIZE function.

   For historical reasons, the functions are inconsistent in whether
   the _N function has an "_n" suffix or has no suffix.  For example,
   BUILT_IN_ATOMIC_LOAD_N is __atomic_load_n, but BUILT_IN_ATOMIC_ADD_FETCH_N
   is __atomic_add_fetch (rather than __atomic_add_fetch_n).  The second
   macro has a SUFFIX argument if the "_n" is (or might be) present.  */

/* DSIZE NAME (volatile void *ptr, DSIZE val).  */

#define DEF_SYNC_BUILTIN_RMW_N(ENUM, NAME, DSIZE, DOFF) \
  DEF_SYNC_BUILTIN_C (ENUM, NAME, BT_FN_##DSIZE##_VPTR_##DSIZE, \
		      ATTR_NOTHROWCALL_LEAF_LIST)

#define DEF_SYNC_BUILTIN_RMW_ALL_N(ENUM, NAME) \
  DEF_SYNC_BUILTIN (ENUM##_N, NAME, BT_FN_VOID_VAR, \
		    ATTR_NOTHROWCALL_LEAF_LIST) \
  FOR_ALL_SYNC_N (ENUM, NAME, DEF_SYNC_BUILTIN_RMW_N)

/* DSIZE NAME (volatile void *ptr, DSIZE val, int memorder).  */

#define DEF_SYNC_BUILTIN_RMW_ORDER_N(ENUM, NAME, DSIZE, DOFF) \
  DEF_SYNC_BUILTIN_C (ENUM, NAME, BT_FN_##DSIZE##_VPTR_##DSIZE##_INT, \
		      ATTR_NOTHROWCALL_LEAF_LIST)

#define DEF_SYNC_BUILTIN_RMW_ORDER_ALL_N(ENUM, NAME, SUFFIX) \
  DEF_SYNC_BUILTIN (ENUM##_N, NAME SUFFIX, BT_FN_VOID_VAR, \
		    ATTR_NOTHROWCALL_LEAF_LIST) \
  FOR_ALL_SYNC_N (ENUM, NAME, DEF_SYNC_BUILTIN_RMW_ORDER_N)

/* DSIZE NAME (volatile void *ptr, DOFF val).  */

#define DEF_SYNC_BUILTIN_RMW_OFF_N(ENUM, NAME, DSIZE, DOFF) \
  DEF_SYNC_BUILTIN_C (ENUM, NAME, BT_FN_##DSIZE##_VPTR_##DOFF, \
		      ATTR_NOTHROWCALL_LEAF_LIST)

#define DEF_SYNC_BUILTIN_RMW_OFF_ALL_N(ENUM, NAME) \
  DEF_SYNC_BUILTIN (ENUM##_N, NAME, BT_FN_VOID_VAR, \
		    ATTR_NOTHROWCALL_LEAF_LIST) \
  FOR_ALL_SYNC_N (ENUM, NAME, DEF_SYNC_BUILTIN_RMW_OFF_N)

/* DSIZE NAME (volatile void *ptr, DOFF val, int memorder).  */

#define DEF_SYNC_BUILTIN_RMW_OFF_ORDER_N(ENUM, NAME, DSIZE, DOFF) \
  DEF_SYNC_BUILTIN_C (ENUM, NAME, BT_FN_##DSIZE##_VPTR_##DOFF##_INT, \
		      ATTR_NOTHROWCALL_LEAF_LIST)

#define DEF_SYNC_BUILTIN_RMW_OFF_ORDER_ALL_N(ENUM, NAME) \
  DEF_SYNC_BUILTIN (ENUM##_N, NAME, BT_FN_VOID_VAR, \
		    ATTR_NOTHROWCALL_LEAF_LIST) \
  FOR_ALL_SYNC_N (ENUM, NAME, DEF_SYNC_BUILTIN_RMW_OFF_ORDER_N)

/* DSIZE NAME (const volatile void *ptr, int memorder).  */

#define DEF_SYNC_BUILTIN_LOAD_ORDER_N(ENUM, NAME, DSIZE, DOFF) \
  DEF_SYNC_BUILTIN_C (ENUM, NAME, BT_FN_##DSIZE##_CONST_VPTR_INT, \
		      ATTR_NOTHROWCALL_LEAF_LIST)

#define DEF_SYNC_BUILTIN_LOAD_ORDER_ALL_N(ENUM, NAME, SUFFIX) \
  DEF_SYNC_BUILTIN (ENUM##_N, NAME SUFFIX, BT_FN_VOID_VAR, \
		    ATTR_NOTHROWCALL_LEAF_LIST) \
  FOR_ALL_SYNC_N (ENUM, NAME, DEF_SYNC_BUILTIN_LOAD_ORDER_N)

/* void NAME (volatile void *ptr, DSIZE val, int memorder).  */

#define DEF_SYNC_BUILTIN_STORE_ORDER_N(ENUM, NAME, DSIZE, DOFF) \
  DEF_SYNC_BUILTIN_C (ENUM, NAME, BT_FN_VOID_VPTR_##DSIZE##_INT, \
		      ATTR_NOTHROWCALL_LEAF_LIST)

#define DEF_SYNC_BUILTIN_STORE_ORDER_ALL_N(ENUM, NAME, SUFFIX) \
  DEF_SYNC_BUILTIN (ENUM##_N, NAME SUFFIX, BT_FN_VOID_VAR, \
		    ATTR_NOTHROWCALL_LEAF_LIST) \
  FOR_ALL_SYNC_N (ENUM, NAME, DEF_SYNC_BUILTIN_STORE_ORDER_N)

/* void NAME (volatile void *ptr).  */

#define DEF_SYNC_BUILTIN_RELEASE_N(ENUM, NAME, DSIZE, DOFF) \
  DEF_SYNC_BUILTIN_C (ENUM, NAME, BT_FN_VOID_VPTR, ATTR_NOTHROWCALL_LEAF_LIST)

#define DEF_SYNC_BUILTIN_RELEASE_NONCAP_N(ENUM, NAME) \
  DEF_SYNC_BUILTIN (ENUM##_N, NAME, BT_FN_VOID_VAR, \
		    ATTR_NOTHROWCALL_LEAF_LIST) \
  FOR_NONCAP_SYNC_N (ENUM, NAME, DEF_SYNC_BUILTIN_RELEASE_N)

/* bool NAME (volatile void *ptr, DSIZE oldval, DSIZE newval).  */

#define DEF_SYNC_BUILTIN_BOOL_CMP_SWAP_N(ENUM, NAME, DSIZE, DOFF) \
  DEF_SYNC_BUILTIN_C (ENUM, NAME, BT_FN_BOOL_VPTR_##DSIZE##_##DSIZE, \
		      ATTR_NOTHROWCALL_LEAF_LIST)

#define DEF_SYNC_BUILTIN_BOOL_CMP_SWAP_ALL_N(ENUM, NAME) \
  DEF_SYNC_BUILTIN (ENUM##_N, NAME, BT_FN_VOID_VAR, \
		    ATTR_NOTHROWCALL_LEAF_LIST) \
  FOR_ALL_SYNC_N (ENUM, NAME, DEF_SYNC_BUILTIN_BOOL_CMP_SWAP_N)

/* DSIZE NAME (volatile void *ptr, DSIZE oldval, DSIZE newval).  */

#define DEF_SYNC_BUILTIN_VAL_CMP_SWAP_N(ENUM, NAME, DSIZE, DOFF) \
  DEF_SYNC_BUILTIN_C (ENUM, NAME, BT_FN_##DSIZE##_VPTR_##DSIZE##_##DSIZE, \
		      ATTR_NOTHROWCALL_LEAF_LIST)

#define DEF_SYNC_BUILTIN_VAL_CMP_SWAP_ALL_N(ENUM, NAME) \
  DEF_SYNC_BUILTIN (ENUM##_N, NAME, BT_FN_VOID_VAR, \
		    ATTR_NOTHROWCALL_LEAF_LIST) \
  FOR_ALL_SYNC_N (ENUM, NAME, DEF_SYNC_BUILTIN_VAL_CMP_SWAP_N)

/* bool NAME (volatile void *ptr, void *expected, DSIZE desired, bool weak,
	      int success_memorder, int failure_memorder).  */

#define DEF_SYNC_BUILTIN_CMP_XCHG_ORDER_N(ENUM, NAME, DSIZE, DOFF) \
  DEF_SYNC_BUILTIN_C (ENUM, NAME, BT_FN_BOOL_VPTR_PTR_##DSIZE##_BOOL_INT_INT, \
		      ATTR_NOTHROWCALL_LEAF_LIST)

#define DEF_SYNC_BUILTIN_CMP_XCHG_ORDER_ALL_N(ENUM, NAME, SUFFIX) \
  DEF_SYNC_BUILTIN (ENUM##_N, NAME SUFFIX, BT_FN_VOID_VAR, \
		    ATTR_NOTHROWCALL_LEAF_LIST) \
  FOR_ALL_SYNC_N (ENUM, NAME, DEF_SYNC_BUILTIN_CMP_XCHG_ORDER_N)

/* Synchronization Primitives.  The "_N" version is the one that the user
   is supposed to be using.  It's overloaded, and is resolved to one of the
   "_1" through "_16" versions, plus some extra casts.  */

DEF_SYNC_BUILTIN_RMW_OFF_ALL_N (BUILT_IN_SYNC_FETCH_AND_ADD,
				"__sync_fetch_and_add")

DEF_SYNC_BUILTIN_RMW_OFF_ALL_N (BUILT_IN_SYNC_FETCH_AND_SUB,
				"__sync_fetch_and_sub")

DEF_SYNC_BUILTIN_RMW_OFF_ALL_N (BUILT_IN_SYNC_FETCH_AND_OR,
				"__sync_fetch_and_or")

DEF_SYNC_BUILTIN_RMW_OFF_ALL_N (BUILT_IN_SYNC_FETCH_AND_AND,
				"__sync_fetch_and_and")

DEF_SYNC_BUILTIN_RMW_OFF_ALL_N (BUILT_IN_SYNC_FETCH_AND_XOR,
				"__sync_fetch_and_xor")

DEF_SYNC_BUILTIN_RMW_OFF_ALL_N (BUILT_IN_SYNC_FETCH_AND_NAND,
				"__sync_fetch_and_nand")

DEF_SYNC_BUILTIN_RMW_OFF_ALL_N (BUILT_IN_SYNC_ADD_AND_FETCH,
				"__sync_add_and_fetch")

DEF_SYNC_BUILTIN_RMW_OFF_ALL_N (BUILT_IN_SYNC_SUB_AND_FETCH,
				"__sync_sub_and_fetch")

DEF_SYNC_BUILTIN_RMW_OFF_ALL_N (BUILT_IN_SYNC_OR_AND_FETCH,
				"__sync_or_and_fetch")

DEF_SYNC_BUILTIN_RMW_OFF_ALL_N (BUILT_IN_SYNC_AND_AND_FETCH,
				"__sync_and_and_fetch")

DEF_SYNC_BUILTIN_RMW_OFF_ALL_N (BUILT_IN_SYNC_XOR_AND_FETCH,
				"__sync_xor_and_fetch")

DEF_SYNC_BUILTIN_RMW_OFF_ALL_N (BUILT_IN_SYNC_NAND_AND_FETCH,
				"__sync_nand_and_fetch")

DEF_SYNC_BUILTIN_BOOL_CMP_SWAP_ALL_N (BUILT_IN_SYNC_BOOL_COMPARE_AND_SWAP,
				      "__sync_bool_compare_and_swap")

DEF_SYNC_BUILTIN_VAL_CMP_SWAP_ALL_N (BUILT_IN_SYNC_VAL_COMPARE_AND_SWAP,
				     "__sync_val_compare_and_swap")

DEF_SYNC_BUILTIN_RMW_ALL_N (BUILT_IN_SYNC_LOCK_TEST_AND_SET,
			    "__sync_lock_test_and_set")

DEF_SYNC_BUILTIN_RELEASE_NONCAP_N (BUILT_IN_SYNC_LOCK_RELEASE,
				   "__sync_lock_release")

DEF_SYNC_BUILTIN (BUILT_IN_SYNC_SYNCHRONIZE, "__sync_synchronize",
		  BT_FN_VOID, ATTR_NOTHROWCALL_LEAF_LIST)

/* __sync* builtins for the C++ memory model.  */

DEF_SYNC_BUILTIN (BUILT_IN_ATOMIC_TEST_AND_SET, "__atomic_test_and_set",
		  BT_FN_BOOL_VPTR_INT, ATTR_NOTHROWCALL_LEAF_LIST)

DEF_SYNC_BUILTIN (BUILT_IN_ATOMIC_CLEAR, "__atomic_clear", BT_FN_VOID_VPTR_INT,
		  ATTR_NOTHROWCALL_LEAF_LIST)

DEF_SYNC_BUILTIN (BUILT_IN_ATOMIC_EXCHANGE,
		  "__atomic_exchange",
		  BT_FN_VOID_VAR, ATTR_NOTHROWCALL_LEAF_LIST)
DEF_SYNC_BUILTIN_RMW_ORDER_ALL_N (BUILT_IN_ATOMIC_EXCHANGE,
				  "__atomic_exchange", "_n")

DEF_SYNC_BUILTIN (BUILT_IN_ATOMIC_LOAD,
		  "__atomic_load",
		  BT_FN_VOID_VAR, ATTR_NOTHROWCALL_LEAF_LIST)
DEF_SYNC_BUILTIN_LOAD_ORDER_ALL_N (BUILT_IN_ATOMIC_LOAD, "__atomic_load", "_n")

DEF_SYNC_BUILTIN (BUILT_IN_ATOMIC_COMPARE_EXCHANGE,
		  "__atomic_compare_exchange",
		  BT_FN_BOOL_VAR, ATTR_NOTHROWCALL_LEAF_LIST)
DEF_SYNC_BUILTIN_CMP_XCHG_ORDER_ALL_N (BUILT_IN_ATOMIC_COMPARE_EXCHANGE,
				       "__atomic_compare_exchange", "_n")

DEF_SYNC_BUILTIN (BUILT_IN_ATOMIC_STORE,
		  "__atomic_store",
		  BT_FN_VOID_VAR, ATTR_NOTHROWCALL_LEAF_LIST)
DEF_SYNC_BUILTIN_STORE_ORDER_ALL_N (BUILT_IN_ATOMIC_STORE,
				    "__atomic_store", "_n")

DEF_SYNC_BUILTIN_RMW_OFF_ORDER_ALL_N (BUILT_IN_ATOMIC_ADD_FETCH,
				      "__atomic_add_fetch")

DEF_SYNC_BUILTIN_RMW_OFF_ORDER_ALL_N (BUILT_IN_ATOMIC_SUB_FETCH,
				      "__atomic_sub_fetch")

DEF_SYNC_BUILTIN_RMW_OFF_ORDER_ALL_N (BUILT_IN_ATOMIC_AND_FETCH,
				      "__atomic_and_fetch")

DEF_SYNC_BUILTIN_RMW_OFF_ORDER_ALL_N (BUILT_IN_ATOMIC_NAND_FETCH,
				      "__atomic_nand_fetch")

DEF_SYNC_BUILTIN_RMW_OFF_ORDER_ALL_N (BUILT_IN_ATOMIC_XOR_FETCH,
				      "__atomic_xor_fetch")

DEF_SYNC_BUILTIN_RMW_OFF_ORDER_ALL_N (BUILT_IN_ATOMIC_OR_FETCH,
				      "__atomic_or_fetch")

DEF_SYNC_BUILTIN_RMW_OFF_ORDER_ALL_N (BUILT_IN_ATOMIC_FETCH_ADD,
				      "__atomic_fetch_add")

DEF_SYNC_BUILTIN_RMW_OFF_ORDER_ALL_N (BUILT_IN_ATOMIC_FETCH_SUB,
				      "__atomic_fetch_sub")

DEF_SYNC_BUILTIN_RMW_OFF_ORDER_ALL_N (BUILT_IN_ATOMIC_FETCH_AND,
				      "__atomic_fetch_and")

DEF_SYNC_BUILTIN_RMW_OFF_ORDER_ALL_N (BUILT_IN_ATOMIC_FETCH_NAND,
				      "__atomic_fetch_nand")

DEF_SYNC_BUILTIN_RMW_OFF_ORDER_ALL_N (BUILT_IN_ATOMIC_FETCH_XOR,
				      "__atomic_fetch_xor")

DEF_SYNC_BUILTIN_RMW_OFF_ORDER_ALL_N (BUILT_IN_ATOMIC_FETCH_OR,
				      "__atomic_fetch_or")

DEF_SYNC_BUILTIN (BUILT_IN_ATOMIC_ALWAYS_LOCK_FREE,
		  "__atomic_always_lock_free",
		  BT_FN_BOOL_SIZE_CONST_VPTR, ATTR_CONST_NOTHROW_LEAF_LIST)

DEF_SYNC_BUILTIN (BUILT_IN_ATOMIC_IS_LOCK_FREE,
		  "__atomic_is_lock_free",
		  BT_FN_BOOL_SIZE_CONST_VPTR, ATTR_CONST_NOTHROW_LEAF_LIST)


DEF_SYNC_BUILTIN (BUILT_IN_ATOMIC_THREAD_FENCE,
		  "__atomic_thread_fence",
		  BT_FN_VOID_INT, ATTR_NOTHROW_LEAF_LIST)

DEF_SYNC_BUILTIN (BUILT_IN_ATOMIC_SIGNAL_FENCE,
		  "__atomic_signal_fence",
		  BT_FN_VOID_INT, ATTR_NOTHROW_LEAF_LIST)

/* This one is actually a function in libatomic and not expected to be
   inlined, declared here for convenience of targets generating calls
   to it.  */
DEF_SYNC_BUILTIN (BUILT_IN_ATOMIC_FERAISEEXCEPT,
		  "__atomic_feraiseexcept",
		  BT_FN_VOID_INT, ATTR_LEAF_LIST)

#undef DEF_SYNC_BUILTIN_CMP_XCHG_ORDER_ALL_N
#undef DEF_SYNC_BUILTIN_CMP_XCHG_ORDER_N
#undef DEF_SYNC_BUILTIN_VAL_CMP_SWAP_ALL_N
#undef DEF_SYNC_BUILTIN_VAL_CMP_SWAP_N
#undef DEF_SYNC_BUILTIN_BOOL_CMP_SWAP_ALL_N
#undef DEF_SYNC_BUILTIN_BOOL_CMP_SWAP_N
#undef DEF_SYNC_BUILTIN_RELEASE_NONCAP_N
#undef DEF_SYNC_BUILTIN_RELEASE_N
#undef DEF_SYNC_BUILTIN_STORE_ORDER_ALL_N
#undef DEF_SYNC_BUILTIN_STORE_ORDER_N
#undef DEF_SYNC_BUILTIN_LOAD_ORDER_ALL_N
#undef DEF_SYNC_BUILTIN_LOAD_ORDER_N
#undef DEF_SYNC_BUILTIN_RMW_OFF_ORDER_ALL_N
#undef DEF_SYNC_BUILTIN_RMW_OFF_ORDER_N
#undef DEF_SYNC_BUILTIN_RMW_OFF_ALL_N
#undef DEF_SYNC_BUILTIN_RMW_OFF_N
#undef DEF_SYNC_BUILTIN_RMW_ORDER_ALL_N
#undef DEF_SYNC_BUILTIN_RMW_ORDER_N
#undef DEF_SYNC_BUILTIN_RMW_ALL_N
#undef DEF_SYNC_BUILTIN_RMW_N
#undef FOR_ALL_SYNC_N
#undef FOR_NONCAP_SYNC_N
