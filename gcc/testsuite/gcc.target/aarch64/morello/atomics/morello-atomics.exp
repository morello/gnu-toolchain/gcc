#  Specific regression driver for AArch64 Morello.
#  Copyright (C) 2021 Free Software Foundation, Inc.
#
#  This file is part of GCC.
#
#  GCC is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3, or (at your option)
#  any later version.
#
#  GCC is distributed in the hope that it will be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCC; see the file COPYING3.  If not see
#  <http://www.gnu.org/licenses/>.  */

# Exit immediately if this isn't an AArch64 target.
if {![istarget aarch64*-*-*] } then {
  return
}

# Load support procs.
load_lib gcc-dg.exp
load_lib c-torture.exp

# Initialize `dg'.
dg-init

if { [check_effective_target_aarch64_capability_any] } {
  set capability_flags ""
} else {
  set capability_flags "-mfake-capability"
}

torture-init
set-torture-options "$C_TORTURE_OPTIONS" [list { } { -mno-outline-atomics } { -moutline-atomics } ]

# Main loop.
gcc-dg-runtest [lsort [glob -nocomplain $srcdir/$subdir/*.\[cCS\]]] \
    "" "$capability_flags -ansi"

torture-finish
# All done.
dg-finish
