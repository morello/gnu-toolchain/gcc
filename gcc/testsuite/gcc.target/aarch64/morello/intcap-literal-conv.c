/* { dg-do compile } */
/* Check that we don't warn here by default.  */
unsigned __intcap f(void)
{
  return -4;
}
