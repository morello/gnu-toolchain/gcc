/* Taken from ASAN testsuite.  */
/* { dg-do run } */
/* { dg-options "-fno-builtin-malloc -fno-builtin-strncpy" } */
/* { dg-shouldfail-purecap "morello bounds" } */

#include <string.h>
#include <stdlib.h>
int main(int argc, char **argv) {
  char *hello = (char*)malloc(6);
  strcpy(hello, "hello");
  char *short_buffer = (char*)malloc(9);
  strncpy(short_buffer, hello, 10);  /* BOOM */
  return short_buffer[8];
}
