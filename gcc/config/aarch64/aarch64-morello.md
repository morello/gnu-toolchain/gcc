;; AArch64 ldp/stp peephole optimizations.
;; Copyright (C) 2021 Free Software Foundation, Inc.
;;
;; This file is part of GCC.
;;
;; GCC is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; GCC is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GCC; see the file COPYING3.  If not see
;; <http://www.gnu.org/licenses/>.

; TODO: check syntax is OK for TARGET_MORELLO.
; TODO: more alternatives.
(define_insn "pointer_plus_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=rk,rk")
	(pointer_plus:CADI
	  (match_operand:CADI 1 "register_operand" "rk,rk")
	  (match_operand:DI 2 "aarch64_pluslong_operand" "Ir,J")))]
  "TARGET_CAPABILITY_ANY"
  "@
  add\\t%0, %1, %2
  sub\\t%0, %1, #%n2"
)

(define_insn "*pointer_plus_lsl_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=r")
	(pointer_plus:CADI
	  (match_operand:CADI 1 "register_operand" "r")
	  (ashift:DI (match_operand:DI 2 "register_operand" "r")
		     (match_operand:QI 3 "aarch64_imm3"))))]
  "TARGET_CAPABILITY_ANY"
  "add\\t%0, %1, %2, lsl %3"
  [(set_attr "type" "alu_shift_imm")]
)

(define_insn "*pointer_plus_multp2_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=rk")
	(pointer_plus:CADI
	  (match_operand:CADI 1 "register_operand" "r")
	  (mult:DI (match_operand:DI 2 "register_operand" "r")
		   (match_operand:DI 3 "aarch64_pwr_imm3"))))]
  "TARGET_CAPABILITY_ANY"
  "add\t%0, %1, %2, lsl %p3"
  [(set_attr "type" "alu_shift_imm")]
)

(define_insn "*pointer_plus_<optab><ALLX:mode>_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=rk")
	(pointer_plus:CADI
	  (match_operand:CADI 1 "register_operand" "r")
	  (ANY_EXTEND:DI (match_operand:ALLX 2 "register_operand" "r"))))]
  "TARGET_CAPABILITY_ANY"
  "add\t%0, %1, %w2, <su>xt<ALLX:size>"
  [(set_attr "type" "alu_ext")]
)

(define_insn "*pointer_plus_<optab><ALLX:mode>_lsl_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=rk")
	(pointer_plus:CADI
	  (match_operand:CADI 1 "register_operand" "r")
	  (ashift:DI
	    (ANY_EXTEND:DI
	      (match_operand:ALLX 2 "register_operand" "r"))
	    (match_operand 3 "aarch64_imm3"))))]
  "TARGET_CAPABILITY_ANY"
  "add\t%0, %1, %w2, <su>xt<ALLX:size> %3"
  [(set_attr "type" "alu_ext")]
)

(define_insn "*pointer_plus_<optab><ALLX:mode>_multp2_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=rk")
	(pointer_plus:CADI
	  (match_operand:CADI 1 "register_operand" "r")
	  (mult:DI
	    (ANY_EXTEND:DI
	      (match_operand:ALLX 2 "register_operand" "r"))
	    (match_operand:DI 3 "aarch64_pwr_imm3"))))]
  "TARGET_CAPABILITY_ANY"
  "add\t%0, %1, %w2, <su>xt<ALLX:size> %p3"
  [(set_attr "type" "alu_ext")]
)

(define_insn "*pointer_plus_<optab>_multp2_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=rk")
	(pointer_plus:CADI
	  (match_operand:CADI 1 "register_operand" "r")
	  (ANY_EXTRACT:DI
	    (mult:DI (match_operand:DI 2 "register_operand" "r")
		     (match_operand:DI 3 "aarch64_pwr_imm3"))
	    (match_operand 4 "const_int_operand")
	    (const_int 0))))]
  "TARGET_CAPABILITY_ANY
   && aarch64_is_extend_from_extract (DImode, operands[3], operands[4])"
  "add\t%0, %1, %w2, <su>xt%e4 %p3"
  [(set_attr "type" "alu_ext")]
)

(define_insn "*pointer_plus_and_lsl_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=rk")
	(pointer_plus:CADI
	  (match_operand:CADI 1 "register_operand" "r")
	  (and:DI
	    (ashift:DI (match_operand:DI 2 "register_operand" "r")
		       (match_operand 3 "aarch64_imm3"))
	    (match_operand:DI 4 "const_int_operand"))))]
  "TARGET_CAPABILITY_ANY
   && aarch64_uxt_size (INTVAL (operands[3]), INTVAL (operands[4])) != 0"
  {
    operands[4] = GEN_INT (aarch64_uxt_size (INTVAL (operands[3]),
					     INTVAL (operands[4])));
    return "add\t%0, %1, %w2, uxt%e4 %3";
  }
  [(set_attr "type" "alu_ext")]
)

(define_insn "*pointer_plus_uxt_multp2_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=rk")
	(pointer_plus:CADI
	  (match_operand:CADI 1 "register_operand" "r")
	  (and:DI
	    (mult:DI (match_operand:DI 2 "register_operand" "r")
		     (match_operand 3 "aarch64_pwr_imm3"))
	    (match_operand 4 "const_int_operand"))))]
  "TARGET_CAPABILITY_ANY
   && aarch64_uxt_size (exact_log2 (INTVAL (operands[3])),
			INTVAL (operands[4])) != 0"
  {
    operands[4] = GEN_INT (aarch64_uxt_size (exact_log2 (INTVAL (operands[3])),
					     INTVAL (operands[4])));
    return "add\t%0, %1, %w2, uxt%e4 %p3";
  }
  [(set_attr "type" "alu_ext")]
)

; TODO: many more alternatives.
(define_insn "*movcadi_aarch64"
  [(set (match_operand:CADI 0 "nonimmediate_operand" "=rk,r,r,m,r,r")
	(match_operand:CADI 1 "aarch64_mov_operand" "rk,Z,m,rZ,Usa,Ush"))]
  "TARGET_CAPABILITY_FAKE"
  "@
   mov\\t%0, %1
   mov\\t%0, xzr
   ldr\\t%0, %1
   str\\t%x1, %0
   adr\\t%0, %c1
   adrp\\t%0, %A1"
)

(define_insn "replace_address_value_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=rk")
        (replace_address_value:CADI
	      (match_operand:CADI 1 "register_operand" "rk")
	      (match_operand:DI 2 "register_operand" "r")))]
  "TARGET_CAPABILITY_ANY"
{
  return TARGET_MORELLO ? "scvalue\t%0, %1, %2" : "mov\t%0, %2";
})

(define_insn "*movcadi_aarch64"
  [(set (match_operand:CADI 0 "nonimmediate_operand" "=rk,r,r,m, r  ,r  ")
	(match_operand:CADI 1 "aarch64_mov_operand"  "rk ,Z,m,rZ,Usa,Ush"))]
  "TARGET_MORELLO"
  "@
  mov\\t%0, %1
  mov\\t%x0, xzr
  ldr\\t%0, %1
  str\\t%B1, %0
  adr\\t%0, %c1
  adrp\\t%0, %A1"
)

(define_insn_and_split "*movcadi_nonzero_aarch64"
  [(set (match_operand:CADI 0 "register_operand"    "=r,r")
	(const:CADI
	  (pointer_plus:CADI
	    (const_null:CADI)
	    (match_operand:DI 1 "const_int_operand" " N,M"))))]
  ""
  "@
   mov\\t%x0, %1
   mov\\t%w0, %1"
   "(!aarch64_move_imm (INTVAL (operands[1]), DImode))"
   [(const_int 0)]
   "{
       aarch64_expand_mov_immediate (operands[0], operands[1]);
       DONE;
    }"
  [(set_attr "type" "mov_imm,mov_imm")
   (set_attr "arch" "*,*")]
)

(define_insn "insv_immcadi"
  [(set (match_operand:CADI 0 "register_operand" "=r")
	 (pointer_plus:CADI
	   (const_null:CADI)
	   (ior:DI
	     (and:DI
	       (match_operand:DI 1 "register_operand" "0")
	       (match_operand 2 "const_int_operand" "n"))
	     (match_operand 3 "const_int_operand" "n"))))]
  "aarch64_get_movk_shift_from_mask (operands[2]) != -1
   && (UINTVAL (operands[3]) & ~UINTVAL (operands[2])) == UINTVAL (operands[3])"
  {
     operands[2] = GEN_INT (aarch64_get_movk_shift_from_mask (operands[2]));
     operands[3] = GEN_INT (UINTVAL (operands[3]) >> UINTVAL (operands[2]));
     return "movk\\t%x0, %X3, lsl %2";
  }
  [(set_attr "type" "mov_imm")]
)

;; CHERI builtins helpers.

(define_insn "cap_base_get_cadi"
  [(set (match_operand:DI 0 "register_operand" "=r")
        (unspec:DI [(match_operand:CADI 1 "register_operand" "rk")]
            UNSPEC_CHERI_BASE_GET))
  ]
  "TARGET_MORELLO"
  "gcbase\\t%0, %1"
)

(define_insn "cap_length_get_cadi"
  [(set (match_operand:DI 0 "register_operand" "=r")
        (unspec:DI [(match_operand:CADI 1 "register_operand" "rk")]
            UNSPEC_CHERI_LEN_GET))
  ]
  "TARGET_MORELLO"
  "gclen\\t%0, %1"
)

(define_insn "cap_representable_alignment_mask_di"
  [(set (match_operand:DI 0 "register_operand" "=r")
        (unspec:DI [(match_operand:DI 1 "register_operand" "r")]
            UNSPEC_CHERI_REPR_ALIGN_MASK)
    )]
  "TARGET_MORELLO"
  "rrmask\\t%0, %1"
)

(define_insn "cap_round_representable_length_di"
  [(set (match_operand:DI 0 "register_operand" "=r")
        (unspec:DI [(match_operand:DI 1 "register_operand" "r")]
           UNSPEC_CHERI_ROUND_REPR_LEN)
    )]
  "TARGET_MORELLO"
  "rrlen\\t%0, %1"
)

(define_insn "cap_bounds_set_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=rk,rk")
        (unspec:CADI [(match_operand:CADI 1 "register_operand" "rk,rk")
          (match_operand:DI 2 "aarch64_scbnds_operand" "r,Ucc")]
            UNSPEC_CHERI_BOUNDS_SET))
  ]
  "TARGET_MORELLO"
  "@
  scbnds\\t%0, %1, %2
  scbnds\\t%0, %1, %2"
)

(define_insn "cap_bounds_set_exact_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=rk")
        (unspec:CADI [(match_operand:CADI 1 "register_operand" "rk")
          (match_operand:DI 2 "register_operand" "r")]
            UNSPEC_CHERI_BOUNDS_SET_EXACT))
  ]
  "TARGET_MORELLO"
  "scbndse\\t%0, %1, %2"
)

(define_insn "cap_bounds_set_maybe_exact"
  [(set (match_operand:CADI 0 "register_operand" "=rk,rk")
        (unspec:CADI [(match_operand:CADI 1 "register_operand" "rk,rk")
          (match_operand:DI 2 "aarch64_scbnds_operand" "r,Ucc")]
            UNSPEC_CHERI_BOUNDS_SET_MAYBE_EXACT))
  ]
  "TARGET_MORELLO"
  "@
  scbndse\\t%0, %1, %2
  scbnds\\t%0, %1, %2"
)

(define_insn "cap_seal_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=rk")
        (unspec:CADI [(match_operand:CADI 1 "register_operand" "rk")
          (match_operand:CADI 2 "register_operand" "rk")]
            UNSPEC_CHERI_SEAL))
  ]
  "TARGET_MORELLO"
  "seal\\t%0, %1, %2"
)

(define_insn "aarch64_cap_clear_perm"
  [(set (match_operand:CADI 0 "register_operand" "=rk,rk")
	(unspec:CADI [(match_operand:CADI 1 "register_operand" "rk,rk")
	  (match_operand:DI 2 "aarch64_clrperm_operand" "r,Ucp")]
	    UNSPEC_CHERI_CLEAR_PERM))
  ]
  "TARGET_MORELLO"
  "@
  clrperm\\t%0, %1, %2
  * return aarch64_output_clrperm_immed (operands);"
)

(define_insn "cap_global_data_get"
  [(set (match_operand:CADI 0 "register_operand" "=rk")
        (unspec:CADI [(const_int 0)]
            UNSPEC_CHERI_GLOBAL_DATA_GET))]
  "TARGET_MORELLO"
  "mrs\\t%0, DDC"
)

(define_expand "cap_perms_and_cadi"
  [(match_operand:CADI 0 "register_operand")
   (match_operand:CADI 1 "register_operand")
   (match_operand:DI 2 "aarch64_reg_or_imm")]
  "TARGET_MORELLO"
  {
    if (CONST_INT_P (operands[2]))
      {
	rtx cand = gen_int_mode (~UINTVAL (operands[2]), DImode);
	if (aarch64_clrperm_immediate (cand, DImode))
	  operands[2] = cand;
	else
	  operands[2] = force_reg (DImode, cand);
      }
    else
      emit_insn (gen_one_cmpldi2 (operands[2], operands[2]));

    emit_insn (gen_aarch64_cap_clear_perm (operands[0], operands[1], operands[2]));
    DONE;
  }
)

(define_insn "cap_offset_get_cadi"
  [(set (match_operand:DI 0 "register_operand" "=r")
        (unspec:DI [(match_operand:CADI 1 "register_operand" "rk")]
            UNSPEC_CHERI_OFFSET_GET))
  ]
  "TARGET_MORELLO"
  "gcoff\\t%0, %1"
)

(define_insn "cap_offset_set_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=rk")
        (unspec:CADI [(match_operand:CADI 1 "register_operand" "rk")
          (match_operand:DI 2 "register_operand" "r")]
            UNSPEC_CHERI_OFFSET_SET))
     ]
  "TARGET_MORELLO"
  "scoff\\t%0, %1, %2"
)

(define_insn "cap_address_get_cadi"
  [(set (match_operand:DI 0 "register_operand" "=r")
        (unspec:DI [(match_operand:CADI 1 "register_operand" "rk")]
            UNSPEC_CHERI_ADDR_GET))
  ]
  "TARGET_MORELLO"
  "gcvalue\\t%0, %1"
)

(define_insn "cap_tag_get_cadi"
  [(set (match_operand:DI 0 "register_operand" "=r")
        (unspec:DI [(match_operand:CADI 1 "register_operand" "rk")]
            UNSPEC_CHERI_TAG_GET))
  ]
  "TARGET_MORELLO"
  "gctag\\t%0, %1"
)

(define_insn "cap_tag_clear_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=rk")
        (unspec:CADI [(match_operand:CADI 1 "register_operand" "rk")]
            UNSPEC_CHERI_TAG_CLEAR))
  ]
  "TARGET_MORELLO"
  "clrtag\\t%0, %1"
)

(define_insn "cap_build_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=rk")
        (unspec:CADI [(match_operand:CADI 1 "register_operand" "rk")
          (match_operand:CADI 2 "register_operand" "rk")]
            UNSPEC_CHERI_CAP_BUILD))
     ]
  "TARGET_MORELLO"
  "build\\t%0, %2, %1"
)

(define_insn "cap_conditional_seal_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=rk")
        (unspec:CADI [(match_operand:CADI 1 "register_operand" "rk")
          (match_operand:CADI 2 "register_operand" "rk")]
            UNSPEC_CHERI_COND_SEAL))
     ]
  "TARGET_MORELLO"
  "cseal\\t%0, %1, %2"
)

(define_insn "cap_type_copy_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=r")
        (unspec:CADI [(match_operand:CADI 1 "register_operand" "r")
          (match_operand:CADI 2 "register_operand" "r")]
            UNSPEC_CHERI_CAP_TYPE_COPY))
     ]
  "TARGET_MORELLO"
  "cpytype\\t%0, %1, %2"
)

(define_insn "aarch64_cap_bit_equality"
  [(set (reg:CC_Z CC_REGNUM)
        (unspec:CC_Z [(match_operand:CADI 0 "register_operand" "rk")
                    (match_operand:CADI 1 "register_operand" "r")]
          UNSPEC_CHERI_BIT_EQ)
        )]
  "TARGET_MORELLO"
  "chkeq\\t%0, %1"
)

(define_expand "cap_equal_exact_cadi"
  [(match_operand:SI 0 "register_operand")
   (match_operand:CADI 1 "register_operand")
   (match_operand:CADI 2 "register_operand")]
  "TARGET_MORELLO"
  {
    rtx chkeq = gen_aarch64_cap_bit_equality (operands[1], operands[2]);
    rtx cc = SET_DEST (chkeq);
    emit_insn (chkeq);
    rtx compare = gen_rtx_EQ (GET_MODE (operands[0]), cc, const0_rtx);
    emit_insn (gen_rtx_SET (operands[0], compare));
    DONE;
  }
)

(define_insn "cap_flags_set_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=rk")
        (unspec:CADI [(match_operand:CADI 1 "register_operand" "rk")
          (match_operand:DI 2 "register_operand" "r")]
            UNSPEC_CHERI_FLAGS_SET))
     ]
  "TARGET_MORELLO"
  "scflgs\\t%0, %1, %2"
)

(define_insn "cap_flags_get_cadi"
  [(set (match_operand:DI 0 "register_operand" "=r")
        (unspec:DI [(match_operand:CADI 1 "register_operand" "rk")]
            UNSPEC_CHERI_FLAGS_GET))
  ]
  "TARGET_MORELLO"
  "gcflgs\\t%0, %1"
)

(define_insn "cap_perms_get_cadi"
  [(set (match_operand:DI 0 "register_operand" "=r")
        (unspec:DI [(match_operand:CADI 1 "register_operand" "rk")]
            UNSPEC_CHERI_PERMS_GET))
  ]
  "TARGET_MORELLO"
  "gcperm\\t%0, %1"
)

(define_insn "aarch64_program_counter_get_<mode>"
  [(set (match_operand:P 0 "register_operand" "=r")
	(unspec_volatile:P [(const_int 0)]
	    UNSPECV_GET_PC))]
  "TARGET_MORELLO"
  "adr\\t%0, #0"
)

(define_insn "cap_pcc_derive"
  [(set (match_operand:CADI 0 "register_operand" "=r")
	(unspec:CADI [
	    (match_operand:DI 1 "register_operand" "r")
	  ] UNSPEC_CHERI_PCC_DERIVE_CAP))]
  "TARGET_MORELLO"
  "cvtp\\t%0, %1"
)

(define_insn "cap_global_data_derive"
  [(set (match_operand:CADI 0 "register_operand" "=r")
	(unspec:CADI [
	  (match_operand:DI 1 "register_operand" "r")
	] UNSPEC_CHERI_DDC_DERIVE_CAP))]
  "TARGET_MORELLO"
  "cvtdz\\t%0, %1"
)

(define_expand "cap_program_counter_get"
  [(match_operand:CADI 0 "register_operand")]
  "TARGET_MORELLO"
  {
    if (Pmode == CADImode)
      emit_insn (gen_aarch64_program_counter_get_cadi (operands[0]));
    else
      {
	gcc_assert (Pmode == DImode && TARGET_CAPABILITY_HYBRID);
	rtx tmp = gen_reg_rtx (DImode);
	emit_insn (gen_aarch64_program_counter_get_di (tmp));
	emit_insn (gen_cap_pcc_derive (operands[0], tmp));
      }
    DONE;
  }
)

(define_insn "cap_seal_entry_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=r")
        (unspec:CADI [(match_operand:CADI 1 "register_operand" "r")]
            UNSPEC_CHERI_SEAL_ENTRY))
  ]
  "TARGET_MORELLO"
  "seal\\t%0, %1, rb"
)

(define_insn "cap_sealed_get_cadi"
  [(set (match_operand:DI 0 "register_operand" "=r")
        (unspec:DI [(match_operand:CADI 1 "register_operand" "rk")]
            UNSPEC_CHERI_SEALED_GET))
  ]
  "TARGET_MORELLO"
  "gcseal\\t%0, %1"
)

(define_insn "aarch64_cap_subset_of"
  [(set (reg:CC_N CC_REGNUM)
        (unspec:CC_N [(match_operand:CADI 0 "register_operand" "rk")
                    (match_operand:CADI 1 "register_operand" "rk")]
          UNSPEC_CHERI_SUBSET_TEST)
        )]
  "TARGET_MORELLO"
  "chkss\\t%0, %1"
)

(define_expand "cap_subset_test_cadi"
  [(match_operand:SI 0 "register_operand")
   (match_operand:CADI 1 "register_operand")
   (match_operand:CADI 2 "register_operand")]
  "TARGET_MORELLO"
  {
    rtx chkss = gen_aarch64_cap_subset_of (operands[1], operands[2]);
    rtx cc = SET_DEST (chkss);
    emit_insn (chkss);
    rtx compare = gen_rtx_LT (GET_MODE (operands[0]), cc, const0_rtx);
    emit_insn (gen_rtx_SET (operands[0], compare));
    DONE;
  }
)

(define_insn "cap_type_get_cadi"
  [(set (match_operand:DI 0 "register_operand" "=r")
        (unspec:DI [(match_operand:CADI 1 "register_operand" "rk")]
            UNSPEC_CHERI_TYPE_GET))
  ]
  "TARGET_MORELLO"
  "gctype\\t%0, %1"
)

(define_insn "cap_unseal_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=r")
        (unspec:CADI [(match_operand:CADI 1 "register_operand" "r")
          (match_operand:CADI 2 "register_operand" "r")]
            UNSPEC_CHERI_UNSEAL))
     ]
  "TARGET_MORELLO"
  "unseal\\t%0, %1, %2"
)

(define_insn "cap_copy_to_high_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=rk")
        (unspec:CADI [(match_operand:CADI 1 "register_operand" "r")
          (match_operand:DI 2 "register_operand" "r")]
            UNSPEC_CHERI_COPY_TO_HIGH))
  ]
  "TARGET_MORELLO"
  "cthi\\t%0, %1, %2"
)

(define_insn "cap_copy_from_high_cadi"
  [(set (match_operand:DI 0 "register_operand" "=r")
        (unspec:DI [(match_operand:CADI 1 "register_operand" "rk")]
            UNSPEC_CHERI_COPY_FROM_HIGH))
  ]
  "TARGET_MORELLO"
  "cfhi\\t%0, %1"
)

(define_insn "aarch64_cap_subset_check_cadi"
  [(set (reg:CC_N CC_REGNUM)
        (unspec:CC_N [(match_operand:CADI 1 "register_operand" "rk")
                      (match_operand:CADI 2 "register_operand" "rk")]
          UNSPEC_CHERI_SUBSET_CHECK_INNER))
   (set (match_operand:CADI 0 "register_operand" "=r")
        (unspec:CADI [(match_dup 1)
                      (match_dup 2)]
          UNSPEC_CHERI_SUBSET_CHECK))]
  "TARGET_MORELLO"
  "chkssu\\t%0, %1, %2"
)

(define_insn "aarch64_ptr_to_cap_offset_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=r")
        (unspec:CADI [(match_operand:CADI 1 "register_operand" "rk")
          (match_operand:DI 2 "register_operand" "r")]
            UNSPEC_CHERI_PTR_TO_CAP_OFFSET))
  ]
  "TARGET_MORELLO"
  "cvtz\\t%0, %1, %2"
)

(define_expand "aarch64_cap_subset_test_unseal_or_null_cadi"
  [(match_operand:CADI 0 "register_operand")
   (match_operand:CADI 1 "register_operand")
   (match_operand:CADI 2 "register_operand")]
  "TARGET_MORELLO"
  {
    rtx chkssu = gen_aarch64_cap_subset_check_cadi (operands[0], operands[1],
                                                    operands[2]);
    emit_insn (chkssu);
    rtx cc = gen_rtx_REG (CCmode, CC_REGNUM);
    rtx cmp_res = gen_reg_rtx (SImode);
    rtx compare_neg = gen_rtx_LT (GET_MODE(cmp_res), cmp_res, const0_rtx);
    emit_insn (gen_cmovcadi_insn (operands[0], compare_neg, cc, operands[0],
                                  const0_rtx));
    DONE;
  }
)

;; Alignment functions.

(define_insn "align_down_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=rk")
        (align_address_down:CADI
               (match_operand:CADI 1 "register_operand" "rk")
               (match_operand:DI 2 "aarch64_pwr_2" "n")))
  ]
  "TARGET_MORELLO"
{
  operands[2] = GEN_INT (exact_log2 (UINTVAL (operands[2])));
  return "alignd\\t%0, %1, #%2";
})

(define_insn "align_address_up_cadi"
  [(set (match_operand:CADI 0 "register_operand" "=rk")
        (align_address_down:CADI
            (pointer_plus:CADI
                (match_operand:CADI 1 "register_operand" "rk")
                (match_operand:DI 2 "const_int_operand" "n"))
        (match_operand:DI 3 "aarch64_pwr_2" "n")))
  ]
  "TARGET_MORELLO && ((INTVAL (operands[2]) + 1) == INTVAL (operands[3]))"
{
  operands[3] = GEN_INT (exact_log2 (INTVAL (operands[3])));
  return "alignu\\t%0, %1, #%3";
})

(define_expand "align_up_cadi"
  [(set (match_operand:CADI 0 "register_operand")
       (align_address_down:CADI
           (pointer_plus:CADI
                (match_operand:CADI 1 "register_operand" "rk")
                (match_dup 3))
           (match_operand:DI 2 "aarch64_pwr_2")))
  ]
  "TARGET_MORELLO"
  {
    operands[3] = plus_constant (DImode, operands[2], -1);
  }
)
