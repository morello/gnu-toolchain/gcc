;; AArch64 ldp/stp peephole optimizations.
;; Copyright (C) 2014-2020 Free Software Foundation, Inc.
;; Contributed by ARM Ltd.
;;
;; This file is part of GCC.
;;
;; GCC is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; GCC is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GCC; see the file COPYING3.  If not see
;; <http://www.gnu.org/licenses/>.

(define_peephole2
  [(set (match_operand 0 "register_operand" "")
	(match_operand 1 "memory_operand" ""))
   (set (match_operand 2 "register_operand" "")
	(match_operand 3 "memory_operand" ""))]
  "aarch64_operands_ok_for_ldpstp (operands, true)"
  [(parallel [(set (match_dup 0) (match_dup 1))
	      (set (match_dup 2) (match_dup 3))])]
{
  aarch64_swap_ldrstr_operands (operands, true);
})

(define_peephole2
  [(set (match_operand 0 "memory_operand" "")
	(match_operand 1 "aarch64_simd_reg_or_zero" ""))
   (set (match_operand 2 "memory_operand" "")
	(match_operand 3 "aarch64_simd_reg_or_zero" ""))]
  "aarch64_operands_ok_for_ldpstp (operands, false)"
  [(parallel [(set (match_dup 0) (match_dup 1))
	      (set (match_dup 2) (match_dup 3))])]
{
  aarch64_swap_ldrstr_operands (operands, false);
})

;; Handle sign/zero extended consecutive load/store.

(define_peephole2
  [(set (match_operand:DI 0 "register_operand" "")
	(sign_extend:DI (match_operand:SI 1 "memory_operand" "")))
   (set (match_operand:DI 2 "register_operand" "")
	(sign_extend:DI (match_operand:SI 3 "memory_operand" "")))]
  "aarch64_operands_ok_for_ldpstp (operands, true)"
  [(parallel [(set (match_dup 0) (sign_extend:DI (match_dup 1)))
	      (set (match_dup 2) (sign_extend:DI (match_dup 3)))])]
{
  aarch64_swap_ldrstr_operands (operands, true);
})

(define_peephole2
  [(set (match_operand:DI 0 "register_operand" "")
	(zero_extend:DI (match_operand:SI 1 "memory_operand" "")))
   (set (match_operand:DI 2 "register_operand" "")
	(zero_extend:DI (match_operand:SI 3 "memory_operand" "")))]
  "aarch64_operands_ok_for_ldpstp (operands, true)"
  [(parallel [(set (match_dup 0) (zero_extend:DI (match_dup 1)))
	      (set (match_dup 2) (zero_extend:DI (match_dup 3)))])]
{
  aarch64_swap_ldrstr_operands (operands, true);
})

;; Handle consecutive load/store whose offset is out of the range
;; supported by ldp/ldpsw/stp.  We firstly adjust offset in a scratch
;; register, then merge them into ldp/ldpsw/stp by using the adjusted
;; offset.

(define_peephole2
  [(match_scratch:DI 8 "r")
   (set (match_operand 0 "register_operand" "")
	(match_operand 1 "memory_operand" ""))
   (set (match_operand 2 "register_operand" "")
	(match_operand 3 "memory_operand" ""))
   (set (match_operand 4 "register_operand" "")
	(match_operand 5 "memory_operand" ""))
   (set (match_operand 6 "register_operand" "")
	(match_operand 7 "memory_operand" ""))
   (match_dup 8)]
  "aarch64_operands_adjust_ok_for_ldpstp (operands, true)"
  [(const_int 0)]
{
  if (aarch64_gen_adjusted_ldpstp (operands, true, UNKNOWN))
    DONE;
  else
    FAIL;
})

(define_peephole2
  [(match_scratch:DI 8 "r")
   (set (match_operand:DI 0 "register_operand" "")
	(sign_extend:DI (match_operand:SI 1 "memory_operand" "")))
   (set (match_operand:DI 2 "register_operand" "")
	(sign_extend:DI (match_operand:SI 3 "memory_operand" "")))
   (set (match_operand:DI 4 "register_operand" "")
	(sign_extend:DI (match_operand:SI 5 "memory_operand" "")))
   (set (match_operand:DI 6 "register_operand" "")
	(sign_extend:DI (match_operand:SI 7 "memory_operand" "")))
   (match_dup 8)]
  "aarch64_operands_adjust_ok_for_ldpstp (operands, true)"
  [(const_int 0)]
{
  if (aarch64_gen_adjusted_ldpstp (operands, true, SIGN_EXTEND))
    DONE;
  else
    FAIL;
})

(define_peephole2
  [(match_scratch:DI 8 "r")
   (set (match_operand:DI 0 "register_operand" "")
	(zero_extend:DI (match_operand:SI 1 "memory_operand" "")))
   (set (match_operand:DI 2 "register_operand" "")
	(zero_extend:DI (match_operand:SI 3 "memory_operand" "")))
   (set (match_operand:DI 4 "register_operand" "")
	(zero_extend:DI (match_operand:SI 5 "memory_operand" "")))
   (set (match_operand:DI 6 "register_operand" "")
	(zero_extend:DI (match_operand:SI 7 "memory_operand" "")))
   (match_dup 8)]
  "aarch64_operands_adjust_ok_for_ldpstp (operands, true)"
  [(const_int 0)]
{
  if (aarch64_gen_adjusted_ldpstp (operands, true, ZERO_EXTEND))
    DONE;
  else
    FAIL;
})

(define_peephole2
  [(match_scratch:DI 8 "r")
   (set (match_operand 0 "memory_operand" "")
	(match_operand 1 "aarch64_simd_reg_or_zero" ""))
   (set (match_operand 2 "memory_operand" "")
	(match_operand 3 "aarch64_simd_reg_or_zero" ""))
   (set (match_operand 4 "memory_operand" "")
	(match_operand 5 "aarch64_simd_reg_or_zero" ""))
   (set (match_operand 6 "memory_operand" "")
	(match_operand 7 "aarch64_simd_reg_or_zero" ""))
   (match_dup 8)]
  "aarch64_operands_adjust_ok_for_ldpstp (operands, false)"
  [(const_int 0)]
{
  if (aarch64_gen_adjusted_ldpstp (operands, false, UNKNOWN))
    DONE;
  else
    FAIL;
})
